/**
 * Created by malobicky on 16.2.17.
 */
import React from 'react';

const ClickCount = ({changes}) => (

  <div className="clickCounter part">
    <p>Clicks</p>
    <p>{changes}</p>
  </div>
);

export default ClickCount;