/**
 * Created by malobicky on 15.2.17.
 */


import React from 'react';


const ButtonChanger = ({
  change,
  disabled,
  color
}) => (<button disabled={disabled}
               className={color + ' Button'}
               onClick={change }/>);
export default ButtonChanger;
