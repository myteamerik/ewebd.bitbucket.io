/**
 * Created by malobicky on 13.2.17.
 */

import React from 'react';
import Box from './Box';

const AllBoxes = ({
  grid,css,
}) => {
  return (
    <div style={css}>
      {grid.map((row, y) => (
        <div key={y}>
          {row.map((col, x) => {
          return (
            <Box
              key={x}
              color={col}
            />
          )
        })}
        </div>
      ))}
    </div>
  )
};


export default AllBoxes;



