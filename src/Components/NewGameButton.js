/**
 * Created by malobicky on 13.2.17.
 */

import React from 'react';


const NewGame = ({newGame}) => (<button className="buttonNewGame part" onClick={newGame} >New Game</button>);

export default NewGame;