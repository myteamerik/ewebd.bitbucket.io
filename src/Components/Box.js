/**
 * Created by malobicky on 13.2.17.
 */

import React from 'react';

const Box = ({
  color,
}) => (
  <div className={color + ' box'} />
);

export default Box;