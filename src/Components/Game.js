/**
 * Created by malobicky on 13.2.17.
 */

import React, {Component} from 'react';
import AllBoxes from './AllBoxes';
import '../style.css';
import NewGameButton from './NewGameButton';
import ChangeColorButton from './ChangeColorButton';
import ClickCount from './ClickCount';
const INIT_COL = 12;
const INIT_ROW = INIT_COL;
// const COLOR = ['red', 'green', 'blue', 'yellow', 'orange'];
const COLOR = ['a1', 'a2', 'a3', 'a4', 'a5'];
const style = {
  width: INIT_COL * 40,
  height: 500,
  margin: '0 auto'
};

const isInArray = (someArray, searchPoint) => {
  return someArray.findIndex((point) => point.x === searchPoint.x && point.y === searchPoint.y) !== -1
};

class Game extends Component {
  constructor() {
    super();
    this.state = {
      grid: this.allColors(),
      click: 0,
      disabled: false,
      visibility: "hidden"
    };
    this.change = this.change.bind(this);
    this.allColors = this.allColors.bind(this);
    this.handleOnClick = this.handleOnClick.bind(this);
    this.stopClick = this.stopClick.bind(this);
  }

  newGame() {
    this.setState({
      grid: this.allColors(),
      click: 0,
      visibility: "hidden"
    });
  };

  randomColor() {
    let items = COLOR;
    let item = items[Math.floor(Math.random() * items.length)];
    return item;
  }

  allColors() {
    const rows = [];
    for (let j = 1; j <= INIT_COL; j++) {
      let ranColors = [];
      for (let i = 1; i <= INIT_ROW; i++) {
        ranColors.push(this.randomColor());
      }
      rows.push(ranColors);
    }
    return rows;
  };

  change(color) {
    this.handleOnClick(this.state.grid, color);
    this.stopClick();
  }

  stopClick() {
    this.setState((state) => ({
      disabled: false
    }));
  }

  handleOnClick(grid, color) {
    const startColor = grid[0][0];
    if (startColor === color) return;
    this.setState((state) => ({
      disabled: true
    }));
    const toCheck = [{x: 0, y: 0}];
    const toChange = [{x: 0, y: 0}];
    while (toCheck.length > 0) {
      const {x, y} = toCheck.shift();
      if (x + 1 !== INIT_COL && startColor === grid[x + 1][y] && !isInArray(toChange, {x: x + 1, y: y})) {
        toChange.push({x: x + 1, y: y});
        toCheck.push({x: x + 1, y: y});
      }
      if (y + 1 !== INIT_COL && startColor === grid[x][y + 1] && !isInArray(toChange, {x: x, y: y + 1})) {
        toChange.push({x: x, y: y + 1});
        toCheck.push({x: x, y: y + 1});
      }
      if (x - 1 !== -1 && startColor === grid[x - 1][y] && !isInArray(toChange, {x: x - 1, y: y})) {
        toChange.push({x: x - 1, y: y});
        toCheck.push({x: x - 1, y: y});
      }
      if (y - 1 !== -1 && startColor === grid[x][y - 1] && !isInArray(toChange, {x: x, y: y - 1})) {
        toChange.push({x: x, y: y - 1});
        toCheck.push({x: x, y: y - 1});
      }
    }

    let previousGrid = grid.slice();
    toChange.forEach(({x, y}, i) => {
      const clone = previousGrid.slice();
      clone[x] = clone[x].slice();
      clone[x][y] = color;

      setTimeout(() => {
        this.setState((state) => ({
          grid: clone
        }));
      }, 10 * i * 1.1);
      previousGrid = clone;

    });
    if (startColor !== color) {
      this.setState((state) => ({
        click: this.state.click + 1,
      }));
    }
    // zla podmienka
    if (toChange.length > 142) {
      this.setState((state) => ({
        visibility: 'visible'
      }))
    }
  }

  render() {
    return (
      <div className="container">
        <h1>Color Flood</h1>
        <div className="header">
          <div className="leftSide">
            <NewGameButton newGame={this.newGame.bind(this)}/>
          </div>
          <div className="middleSide">
            <ChangeColorButton disabled={this.state.disabled} change={this.change.bind(null, COLOR[0])}
                               color={COLOR[0]}/>
            <ChangeColorButton disabled={this.state.disabled} change={this.change.bind(null, COLOR[1])}
                               color={COLOR[1]}/>
            <ChangeColorButton disabled={this.state.disabled} change={this.change.bind(null, COLOR[2])}
                               color={COLOR[2]}/>
            <ChangeColorButton disabled={this.state.disabled} change={this.change.bind(null, COLOR[3])}
                               color={COLOR[3]}/>
            <ChangeColorButton disabled={this.state.disabled} change={this.change.bind(null, COLOR[4])}
                               color={COLOR[4]}/>
          </div>
          <div className="rightSide">
            <ClickCount changes={this.state.click}/>
          </div>
        </div>


        <div className="allBoxes">
          <div className={this.state.visibility}><h1>U WON</h1></div>
          <AllBoxes css={style} grid={this.state.grid}/>

        </div>

      </div>

    );

  }

}
export default Game;




